import pygame, sys, random
pygame.init()
pygame.font.init()
pygame.joystick.init()
joysticks = [pygame.joystick.Joystick(x) for x in range(pygame.joystick.get_count())]
for joystick in joysticks:
    joystick.init()
tailLength=6
frameTime = 100
zoom = 3
size = width, height = 320 * zoom, 180 * zoom
cellsize = cellWidth, cellHeight = 10 * zoom,10 * zoom
columns = width//cellWidth
rows= height//cellHeight
black = 0, 0, 0
white = 255,255,255
red = 255,0,0
yellow= 255,255,0
blue = 0,0,255
green = 0,255,0
direction=1
gameState = False
score=0
blocks = []
while len(blocks)<rows:
    blocks.append(0)
tail=[]
while len(tail)<tailLength:
    tail.append(columns//2)
screen = pygame.display.set_mode(size)
pygame.display.set_caption('JetLag 2018')
font = pygame.font.SysFont('Terminal',cellHeight*2)
nextFrame = pygame.time.get_ticks() + frameTime
while 1:
    for event in pygame.event.get():
        if event.type == pygame.QUIT: 
            sys.exit()
        elif event.type == pygame.JOYAXISMOTION:
            if gameState and event.axis==0:
                if event.value<-0.5:
                    direction=-1
                elif event.value>0.5:
                    direction=1
        elif event.type == pygame.JOYBUTTONDOWN:
            if not gameState and event.button==0:
                score=0
                for index in range(rows):
                    blocks[index]=0
                for index in range(tailLength):
                    tail[index]=columns//2
                nextFrame = pygame.time.get_ticks() + frameTime
                gameState = True
        elif event.type == pygame.KEYDOWN:
            if gameState:
                if event.key == pygame.K_LEFT:
                    direction = -1
                elif event.key == pygame.K_RIGHT:
                    direction = 1
            else:
                if event.key == pygame.K_SPACE:
                    score=0
                    for index in range(rows):
                        blocks[index]=0
                    for index in range(tailLength):
                        tail[index]=columns//2
                    nextFrame = pygame.time.get_ticks() + frameTime
                    gameState=True
    if gameState and pygame.time.get_ticks()>=nextFrame:
        score+=1
        nextFrame += frameTime
        for row in range(rows-1):
            blocks[row]=blocks[row+1]
        blocks[rows-1]=random.randint(1,columns-1)
        for row in range(tailLength-1):
            tail[row]=tail[row+1]
        tail[tailLength-1]+=direction
        if tail[tailLength-1]==0 or tail[tailLength-1]==columns-1 or tail[tailLength-1]==blocks[tailLength-1]:
            gameState=False
    screen.fill(black)
    for row in range(rows):
        screen.fill(white,pygame.Rect(blocks[row]*cellWidth,row*cellHeight,cellWidth,cellHeight))
    screen.fill(blue,pygame.Rect(0,0,cellWidth,height))
    screen.fill(blue,pygame.Rect(width-cellWidth,0,cellWidth,height))
    for row in range(tailLength):
        if row==tailLength-1:
            screen.fill(red,pygame.Rect(tail[row]*cellWidth,row*cellHeight,cellWidth,cellHeight))
        else:
            screen.fill(yellow,pygame.Rect(tail[row]*cellWidth,row*cellHeight,cellWidth,cellHeight))
    surface = font.render(str(score),False,green)
    screen.blit(surface,(cellWidth,0))
    pygame.display.flip()